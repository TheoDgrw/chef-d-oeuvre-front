import React from 'react';

import Nav from '../Nav';

export default function Home() {

    return <>
    <Nav />
        <div className="container">
            <p>
                Le freeman est un jeu qui se joue d'habitude à l'oral.
                Le but du jeu est de relier deux acteurs par un chemin de films et d'acteurs.
                Par exemple je cherche à relier Brad Pitt et Georges Clooney:
                - Brad Pitt a joué avec Georges Clooney dans Ocean's Eleven
                On peut aussi faire un chemin plus long, par exemple:
                - Brad Pitt a joué avec Matt Damon dans Ocean's Eleven et Matt Damon a joué avec Georges Clooney dans Ocean's twelve
                Vous pouvez créer des ramifications dans le chemin à tout moment créant une sorte d'arbre, par exmple:
            </p>

            <ul>Brad Pitt
                <li>
                    a joué dans Ocean's Eleven
                    <ul>
                        <li>avec Georges Clooney</li>
                    </ul>
                </li>
                <li>
                    a joué dans L'étrange histoire de Benjamin Button
                    <ul>
                        <li>avec Cate Blanchett</li>
                        <li>avec ...</li>
                    </ul>
                </li>
            </ul>

            Une fois que le chemin est fait, les deux acteurs ont été reliés, la partie est gagnée.
        </div>
    </>
}