import { configureStore } from '@reduxjs/toolkit';
import tokenReducer from '../game/tokenManagement/TokenSlice';

export default configureStore({
    reducer: {
        token: tokenReducer,
    },
});