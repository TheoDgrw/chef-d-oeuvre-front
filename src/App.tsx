import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import store from './store';

import './App.css';

import Home from './Home';
import Register from './Register';
import Board from "./game/Board";
import Login from './login';

function App() {

    useEffect(() => {
        const body = document.querySelector('body');
        if (body) {
            body.style.minHeight = window.innerHeight.toString() + 'px';
            body.style.backgroundColor = '#2C2C32';
        }
    }, [])

    return (
        <Provider store={store}>
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route path='/login' component={Login} />
                    <Route path='/game' component={Board} />
                    <Route path='/register' component={Register} />
                </Switch>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
