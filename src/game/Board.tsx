import React, { useState, useEffect } from "react";
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { selectToken } from './tokenManagement/TokenSlice';
import useGetRandomActors from "./apiCalls/useGetRandomActors";
import GameBloc from "./GameBloc/GameBloc";
import Nav from '../Nav';

export default function Board() {
    const [loading, setLoading] = useState(true);

    const history = useHistory();

    const token = useSelector(selectToken);

    console.log('token', token);

    const actors = useGetRandomActors();

    useEffect(() => {
        if (actors) {
            setLoading(false);
        }
    }, [actors]);

    if (token === null) {
        history.push('/login');
    }

    if (loading) {
        return <h1>Loading...</h1>;
    }

    return (
        <>
            <Nav />
            <GameBloc actors={actors} />
        </>
    );
}