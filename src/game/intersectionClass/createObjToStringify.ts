import Intersection from "./Intersection";

export default function createObjToStringify(intersection: Intersection, res = {}): any {

    let id = intersection.parent ? intersection.parent.id : 'noParent';
    res[id] = {
        parent: intersection.parent ? intersection.parent.id : null,
        finalActor: intersection.finalActor,
        name: intersection.name,
        infos: intersection.infos,
        isMovie: intersection.isMovie,
        lvl: intersection.lvl,
        childrens: intersection.childrens.length > 0 ? intersection.childrens.map(children => {
            console.log(children)
            return createObjToStringify(children).id;
        })
        : null
    }

    return res;
}