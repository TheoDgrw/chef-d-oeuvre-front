import { postWinIntersection } from '../../myApiCalls/postWinIntersection';

export default class Intersection {
    parent: Intersection | null;
    childrens: Intersection[];
    nbOfChildrens: number;
    isMovie: Boolean;
    name: string;
    ctx: CanvasRenderingContext2D;
    lvl: number[];
    coord: number[];
    infos: any[] | null;
    id: string;
    apiKey: string;
    addAllowed: Boolean;
    domElement: HTMLElement | null;
    bloc: HTMLElement;
    viewPort: HTMLDivElement | null;
    canvas: HTMLCanvasElement | null;
    content: HTMLDivElement | null;
    finalActor;
    scrollBooster;

    constructor(
        parent: Intersection | null,
        isMovie: Boolean,
        name: string,
        ctx: CanvasRenderingContext2D,
        lvl: number[],
        bloc: HTMLElement
    ) {
        this.parent = parent;
        this.childrens = [];
        this.nbOfChildrens = 0;
        this.isMovie = isMovie;
        this.name = name;
        this.ctx = ctx;
        this.lvl = lvl;
        this.coord = [0, 0];
        this.infos = null;
        this.id = "";
        this.apiKey = "9e09d751ac6be95b3da98f39f17d3e6a";
        this.domElement = this.parent ? this.createElement(bloc) : null;
        this.addAllowed = false;
        this.bloc = bloc;
        this.viewPort = this.parent ? this.parent.viewPort : null;
        this.canvas = this.parent ? this.parent.canvas : null;
        this.content = this.parent ? this.parent.content : null;
        this.finalActor = this.parent ? this.parent.finalActor : null;
        this.scrollBooster = this.parent ? this.parent.scrollBooster : null;
    }

    getLvl(): number[] {
        if (this.childrens) {
            return this.childrens[this.childrens.length - 1].getLvl();
        } else {
            return this.lvl;
        }
    }

    setLvl(): void {
        if (this.parent) {
            this.lvl = this.parent.lvl.concat(this.parent.childrens.indexOf(this));
        } else {
            this.lvl = [0];
        }
        this.childrens.forEach(children => {
            children.setLvl();
            children.setCoord();
        });
    }

    setCoord(): void {
        if (this.parent) {
            let x: number = this.parent.coord[0] + 250;
            let yInfo1 = this.parent.coord[1] + 130 * this.lvl[this.lvl.length - 1]
            let lastChild: Intersection[] = [];
            let prevSibling = this.prevSibling();
            if (prevSibling) {
                prevSibling.getLastChild(lastChild);
            }
            if (lastChild.length > 0) {
                const lastYaxis = lastChild[0].coord[1];
                yInfo1 = lastYaxis + 130;
            }
            let y = yInfo1 !== null ? yInfo1 : 0;
            this.coord = [x, y];
            if (this.domElement) {
                this.domElement.style.left = x + 'px';
                this.domElement.style.top = y + 'px';
            }

        } else {
            this.coord = [0, 0];
            if (this.domElement) {
                this.domElement.style.left = '0px';
                this.domElement.style.top = '0px';
            }
        }
    }

    updateCoord(): void {
        this.setCoord();
        let res: Intersection[] = [];
        this.getFirstIntersection().getLastChild(res);
        if (this === res[0]) {
            this.setCanvasSize();
            this.getFirstIntersection().updateCanvas();
        }
        this.childrens.forEach(children => {
            children.updateCoord();
        })
    }

    updateCanvas(): void {
        this.draw();
        this.childrens.forEach(children => {
            children.updateCanvas();
        });
    }

    addChildren(name: string, isItAMovie: Boolean): Intersection {
        const isMovie: Boolean = isItAMovie;
        const newLvl: number = this.childrens.length
        const newChild = new Intersection(
            this,
            isMovie,
            name,
            this.ctx,
            [...this.lvl, newLvl],
            this.bloc
        );
        this.childrens = [
            ...this.childrens,
            newChild
        ];
        newChild.setCoord();
        this.getFirstIntersection().updateCoord();
        this.updateScrollBooster();

        return newChild;
    }

    deleteAllChildrens(): void {
        this.childrens = [];
    }

    deleteChildren(name: string) {
        this.childrens = this.childrens.filter((children => {
            return children.name !== name;
        }))
    }

    draw(): void {
        if (this.parent !== null) {
            this.ctx.beginPath();
            this.ctx.strokeStyle = 'white';
            this.ctx.moveTo(this.parent.coord[0] + 100, this.parent.coord[1] + 45);
            this.ctx.lineTo(this.coord[0] - 30, this.parent.coord[1] + 45);
            this.ctx.lineTo(this.coord[0] - 30, this.coord[1] + 45);
            this.ctx.lineTo(this.coord[0], this.coord[1] + 45);
            this.ctx.stroke();
        }
    }

    shift() {
        // TODO: décaler d'un cran vers le haut
    }

    setNbOfChildrens(): void {
        this.nbOfChildrens = this.childrens.length;
    }

    prevSiblingChildNb(): number {
        if (this.parent && this.parent.childrens[this.parent.childrens.indexOf(this) -1]) {
            // console.log(this.parent.childrens[this.parent.childrens.indexOf(this) -1].childrens.length);
            return this.parent.childrens[this.parent.childrens.indexOf(this) -1].childrens.length;
        }
        return 0;
    }

    prevSibling(): Intersection | null {
        if (this.parent && this.parent.childrens[this.parent.childrens.indexOf(this) -1]) {
            return this.parent.childrens[this.parent.childrens.indexOf(this) -1];
        }
        return null;
    }

    getLastChild(res): void {
        if (this.childrens.length > 0) {
            if (this.childrens[this.childrens.length - 1].childrens) {
                this.childrens[this.childrens.length - 1].getLastChild(res)
            } else {
                res.push(this.childrens[this.childrens.length - 1])
            }
        } else {
            res.push(this);
        }
    }

    getIntersection(lvl: number[], localChildren): void {
        // TODO: return intersection on clic
        if (this.lvl.toString() === lvl.toString()) {
            localChildren.push(this);
        }
        this.childrens.forEach(children => {
            if (children.lvl.toString() === lvl.toString()) {
                localChildren.push(children);
            } else {
                children.getIntersection(lvl, localChildren);
            }
        });
    }

    createView(res: Intersection[]): void {
        if (this.parent === null) {
            res.push(this);
        }
        if (this.childrens) {
            this.childrens.forEach(children => {
                res.push(children);
                children.createView(res);
            });
        }
    }

    getFirstIntersection(): Intersection {
        if (this.parent) {
            return this.parent.getFirstIntersection();
        } else {
            return this;
        }
    }

    // TODO: merge function createElement() and createFirstElement()
    createElement(bloc: HTMLElement): HTMLElement {
        const newElement: HTMLDivElement = document.createElement('div');
        newElement.style.display = 'flex';
        newElement.style.flexDirection = 'column';
        newElement.style.width = '200px';
        newElement.style.border = '1px solid white';
        newElement.style.height = '120px';
        const label: HTMLLabelElement = document.createElement('label');
        label.setAttribute('for', this.lvl.toString());
        label.innerHTML = this.name;
        const select: HTMLSelectElement = document.createElement('select');
        select.setAttribute('id', this.lvl.toString());
        select.style.width = '190px';
        select.style.backgroundColor = 'white';
        newElement.appendChild(label);
        newElement.appendChild(select);
        const intersection: Intersection = this;
        this.parent?.infos?.map(info => {
            let option = document.createElement('option');
            option.setAttribute('value', info.id);
            if (intersection.isMovie) {
                option.innerHTML = info.original_title;
            } else {
                option.innerHTML = info.name;
            }
            select.appendChild(option);
        });
        bloc.appendChild(newElement);
        const add: HTMLButtonElement = document.createElement('button');
        add.style.marginTop = '5px';
        add.classList.add('btn', 'btn-light');
        add.innerHTML = "Add";
        add.addEventListener('click', event => {
            intersection.addChildren("", !intersection.isMovie);
        });
        add.setAttribute('disabled', 'true');
        newElement.appendChild(add);
        const valid: HTMLButtonElement = document.createElement('button');
        valid.style.marginTop = '3px'
        valid.classList.add('btn', 'btn-light');
        valid.innerHTML = "Submit";
;        valid.addEventListener('click', () => {
            intersection.setInfos(select.options[select.selectedIndex].text, select.options[select.selectedIndex].value);
        });
        newElement.appendChild(valid);
        newElement.style.left = this.coord[0] + 'px';
        newElement.style.top = this.coord[1] + 'px';
        newElement.style.position = 'absolute';

        return newElement;
    }

    createFirstElement(bloc: HTMLElement): HTMLElement {
        const newElement: HTMLDivElement = document.createElement('div');
        newElement.style.display = 'flex';
        newElement.style.flexDirection = 'column';
        newElement.style.width = '200px';
        newElement.style.border = '1px solid white';
        newElement.style.height = '90px';
        const p: HTMLParagraphElement = document.createElement('p');
        // label.setAttribute('for', this.lvl.toString());
        p.innerHTML = this.name;
        p.classList.add('text-center');
        // const select: HTMLSelectElement = document.createElement('select');
        // select.setAttribute('id', this.lvl.toString());
        // select.style.width = '190px';
        newElement.appendChild(p);
        // newElement.appendChild(select);

        // this.parent?.infos?.map(info => {
        //     let option = document.createElement('option');
        //     option.setAttribute('value', info.id);
        //     option.innerHTML = info.name;
        //     select.appendChild(option);
        // });
        const add: HTMLButtonElement = document.createElement('button');
        add.innerHTML = "Add";
        const intersection: Intersection = this;
        add.addEventListener('click', event => {
            intersection.addChildren("", !intersection.isMovie);
        });
        add.setAttribute('disabled', 'true');
        add.classList.add('btn', 'btn-light')
        newElement.appendChild(add);
        const valid: HTMLButtonElement = document.createElement('button');
        valid.innerHTML = "Submit";
        // valid.addEventListener('click', () => {
        //     console.log('coucou')
        //     console.log(select.options[select.selectedIndex].text)
        //     intersection.setInfos(select.options[select.selectedIndex].text, select.options[select.selectedIndex].value);
        // });

        if (this.parent === null) {
            this.setInfos(this.name, this.id);
        }
        bloc.appendChild(newElement);

        return newElement;
    }

    setInfos(name: string, id: string) {
        console.log('name', name)
        console.log('id', id);
        this.name = name;
        this.id = id;
        this.fetchInfos();
    }

    fetchInfos(): Promise<Object[] | void> {
        const url: string = this.isMovie ?
            `https://api.themoviedb.org/3/movie/${this.id}/credits?api_key=${this.apiKey}`
            : `https://api.themoviedb.org/3/person/${this.id}/movie_credits?api_key=${this.apiKey}`
        ;
        let intersection: Intersection = this;    
        return fetch(url, {
            method: 'GET'
        })
            .then(res => res.json())
            .then(data => {
                // console.log(data);
                intersection.domElement?.querySelector('button')?.removeAttribute('disabled');
                intersection.infos = data.cast;
                intersection.checkVictory();
                return data.cast;
            })
        ;
    }

    setCanvasSize(): void {
        if (this.canvas && this.content) {
            this.ctx.canvas.width = this.content.scrollWidth;
            this.ctx.canvas.height = this.content.scrollHeight;
        }
    }

    updateScrollBooster(): void {
        this.scrollBooster.updateMetrics();
    }

    checkVictory(): void {
        if (this.isMovie) {
            this.infos?.forEach(info => {
                if (info.id === this.finalActor?.id) {
                    this.executeOnWin();
                    return alert('Well Done !');
                }
            })
        }
    }

    executeOnWin() {
        return postWinIntersection(this.getFirstIntersection());
    }
};