import { useState, useEffect } from "react";

export default function useSearchActors(search): null | Object[] {
    const [actors, setActors] = useState<null | Object[]>(null);

    const apiKey = "9e09d751ac6be95b3da98f39f17d3e6a";
    const url = `https://api.themoviedb.org/3/search/person?api_key=${apiKey}&query=${search}`;

    useEffect(() => {
        async function fetchActor() {
            fetch(
                url, {
                    method: 'GET'
                }
            )
                .then(res => res.json())
                .then(data => setActors(data))
            ;
        };

        fetchActor();
    }, []);

    return actors;
}