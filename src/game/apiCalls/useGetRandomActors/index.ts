import { useState, useEffect } from "react";
import { getRandomInt } from "./functions";

export default function useGetRandomActors(): null | Array<Object> {
    const [randomActors, setRandomActors] = useState<Array<Object> | null>(null);

    const apiKey = "9e09d751ac6be95b3da98f39f17d3e6a";
    const url = "https://api.themoviedb.org/3";

    useEffect(() => {
        async function fetchActors() {
            const res: any = await fetch(
                `${url}/person/popular?api_key=${apiKey}&page=${getRandomInt(5)}`, {
                    method: 'GET'
                }
            );

            if (res.status === 422) {
                fetchActors();
            } else {
                const data = await res.json();
                const randomPopularActors: Object[] = data.results;
                const randomIndex = getRandomInt(randomPopularActors.length);
                const randomIndex2 = getRandomInt(randomPopularActors.length);
                const randomPopularActor = randomPopularActors[randomIndex];
                const randomPopularActor2 = randomPopularActors[randomIndex2];
                
                setRandomActors([randomPopularActor, randomPopularActor2]);
            }
        }

        fetchActors();
    }, []);

    return randomActors;
}