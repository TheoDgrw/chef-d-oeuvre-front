import { useState, useEffect } from "react";

export default function useGetCast(intersection, callback, rerender, setRerender): null | Object[] {
    const [infos, setInfos] = useState<null | Object[]>(null);

    const apiKey = "9e09d751ac6be95b3da98f39f17d3e6a";
    const url = intersection.isMovie ?
        `https://api.themoviedb.org/3/movie/${{...intersection}.id}/credits?api_key=${apiKey}`
        : `https://api.themoviedb.org/3/person/${{...intersection}.id}/movie_credits?api_key=${apiKey}`
    ;

    // const needed = intersection.infos === null ? true : false;

    useEffect(() => {
        async function fetchCast() {
            fetch(url, {
                method: 'GET'
            })
                .then(res => res.json())
                .then(data => {
                    console.log(intersection)
                    console.log(intersection.infos)
                    console.log(intersection.submitted)
                    console.log(!intersection.infos && intersection.submitted === true)
                    intersection.infos = data.cast
                    callback(intersection.getFirstIntersection());
                    setRerender(!rerender);
                    return data.cast
                })
            ;
        }

        if (intersection)
        fetchCast();
    }, [rerender]);

    return infos;
}