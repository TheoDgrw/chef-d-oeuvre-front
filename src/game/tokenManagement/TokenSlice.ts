import { createSlice } from "@reduxjs/toolkit";

export const slice = createSlice({
    name: 'token',
    initialState: {
        token: localStorage.getItem('token')
    },
    reducers: {
        setToken: (state, action) => {
            state.token = action.payload;
        },
    },
});

export const { setToken } = slice.actions;

export const selectToken = state => state.token.token;

export default slice.reducer;