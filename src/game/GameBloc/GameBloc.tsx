import React, { useRef, useEffect } from "react";
import Intersection from "../intersectionClass/Intersection";
import ScrollBooster from 'scrollbooster';

export default function GameBloc({actors}) {

    const blocRef = useRef(null);
    const canvasRef = useRef(null);
    const viewPortRef = useRef(null);
    const contentRef = useRef(null);

    useEffect(() => {
        const bloc: HTMLElement = blocRef.current;
        const canvas = canvasRef.current;
        const ctx: CanvasRenderingContext2D = canvas.getContext('2d');

        const firstIntersection = new Intersection(null, false, actors[0].name, ctx, [0], bloc);
        firstIntersection.name = actors[0].name;
        firstIntersection.id = actors[0].id;
        firstIntersection.viewPort = viewPortRef.current;
        firstIntersection.canvas = canvas;
        firstIntersection.content = contentRef.current;
        firstIntersection.finalActor = actors[1];
        firstIntersection.scrollBooster = new ScrollBooster({
            viewport: viewPortRef.current,
            content: contentRef.current,
            scrollMode: "transform"
        });
        firstIntersection.domElement = firstIntersection.createFirstElement(bloc);
    });

    return <>
        <div className="d-flex justify-content-between boardGame">
            <div className="viewport" ref={viewPortRef} style={{ maxWidth: window.innerWidth, height: window.innerHeight, overflow: 'hidden'}}>
                <div ref={contentRef}>
                    <div className="content" ref={blocRef} style={{position: 'absolute'}}></div>
                    <canvas ref={canvasRef}></canvas>
                </div>
            </div>
            <div style={{
                height: '120px',
                width: '200px',
                border: 'solid white 1px'
            }}>
                <p className="text-center">{actors[1].name}</p>
            </div>
        </div>
    </>
}