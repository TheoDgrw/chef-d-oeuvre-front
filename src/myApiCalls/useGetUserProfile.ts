import { useState, useEffect } from 'react';

export default function useGetUserProfile() {
    const [infos, setInfos] = useState()

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            fetch('http://localhost:3000/api/profile', {
                method: 'GET',
                headers: [['Authorization', token]]
            })
                .then(res => res.json())
                .then(data => setInfos(data))
            ;
        }
    }, []);

    return infos;
}