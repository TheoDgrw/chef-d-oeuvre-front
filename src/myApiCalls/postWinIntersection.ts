import Intersection from "../game/intersectionClass/Intersection";

export async function postWinIntersection(firstIntersectionWin: Intersection): Promise<Object> {
    const intersection: string = JSON.stringify({...firstIntersectionWin}, (k, v) => {
        if (
            k === 'parent'
            || k === 'domElement'
            || k === 'content'
            || k === 'viewPort'
            || k === 'canvas'
            || k === 'bloc'
            || k === 'scrollBooster'
            || k === 'ctx'
        ) {
                return
        };
        console.log(k);

        return v;
    });
    console.log(intersection);
    const token: string | null = localStorage.getItem('token');

    if (token) {
        return fetch('http://localhost:3000/api/intersections', {
            method:'POST',
            headers: [['Authorization', 'Token ' + token], ['Content-Type', 'application/json']],
            body: intersection
        })
            .then(res => res.json())
            .then(data => data)
        ;
    }
    console.error('pas de token dans le local storage !');

    return {error: 'no token in database'};
}