import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { useHistory } from 'react-router-dom';

import Nav from '../Nav';
import { setToken } from '../game/tokenManagement/TokenSlice';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);
    const [redirect, setRedirect] = useState(false);

    const dispatch = useDispatch();
    const history = useHistory();

    const handleSubmit = async (): Promise<void> => {
        const body: Object = {
            user: {
                password: password,
                email: email
            }
        }
        const res = await fetch('http://localhost:3000/api/users/login', {
            method: 'POST',
            body: JSON.stringify(body),
            headers: [['Content-Type', 'application/json']]
        });

        const data = await res.json();

        if (res.status === 422) {
            let newError: string = '';
            for (const property in data.errors) {
                newError += property + ' ' + data.errors[property];
            }
            setError(newError);
        } else {
            localStorage.setItem('token', data.user.token);
            localStorage.setItem('exp', moment().format())
            dispatch(setToken(data.user.token));
            setRedirect(true);
        }
    }

    const handleChange = (event) => {
        switch (event.target.name) {
            case 'email':
                setEmail(event.target.value);
                break;
            case 'password':
                setPassword(event.target.value);
                break;
        }
    }

    if (redirect) {
        history.length !== 0 ? history.goBack() : history.push('/game');
    }

    return <>
        <Nav />
        <div className="container" style={{
            backgroundColor: 'white',
            border: 'solid white 1px',
            borderRadius: '5px',
            width: '40rem',
            marginTop: '10rem'
        }}>
            {error ? <p>{error}</p> : null}
            <div>
                <label htmlFor="email" style={{color: 'white'}}></label>
                <input className="row mx-auto" id="email" onChange={handleChange} type="text" name="email"/>
            </div>
            <div>
                <label htmlFor="password"></label>
                <input id="password" className="row mx-auto" onChange={handleChange} type="text" name="password"/>
            </div>
            <div className="row mx-auto">
                <button className="btn btn-light btn-block" onClick={handleSubmit}>Login</button>
            </div>
        </div>
    </>
}