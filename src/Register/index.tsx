import React, { useState } from 'react';

export default function Register() {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleChange = (event) => {
        switch (event.target.name) {
            case 'username':
                setUsername(event.target.value);
                break;
            case 'email':
                setEmail(event.target.value);
                break;
            case 'password':
                setPassword(event.target.value);
                break;
        }
    }

    const handleSubmit = async () => {
        const res = await fetch('http://localhost:3000/api/users', {
            method: 'POST',
            headers: [['Content-Type', 'application/json']],
            body: `{
                "user": {
                    "email": "${email}",
                    "username": "${username}",
                    "password": "${password}"
                }
            }`
        });
        const data = await res.json();
        console.log('data', data);
    }

    return <>
        <input onChange={handleChange} type="text" name="username"/>
        <input onChange={handleChange} type="text" name="email"/>
        <input onChange={handleChange} type="text" name="password"/>
        <button onClick={handleSubmit}>Register</button>
    </>
}